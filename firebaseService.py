import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import os

from google.oauth2 import service_account
serviceAccountKey = {
  "type": os.environ.get('type'),
  "project_id": os.environ.get('project_id'),
  "private_key_id": os.environ.get('private_key_id'),
  "private_key": os.environ.get('private_key').replace('\\n', '\n'),
  "client_email": os.environ.get('client_email'),
  "client_id": os.environ.get('client_id'),
  "auth_uri": os.environ.get('auth_uri'),
  "token_uri": os.environ.get('token_uri'),
  "auth_provider_x509_cert_url": os.environ.get('auth_provider_x509_cert_url'),
  "client_x509_cert_url": os.environ.get('client_x509_cert_url')
}

cred = credentials.Certificate(serviceAccountKey)
firebase_admin.initialize_app(cred)

db = firestore.client()

dogs_ref = db.collection(u'dogs')

def getDogById(id):
    dogs = dogs_ref.stream()
    for dog in dogs:
        if(dog.to_dict()["id"]==id):
            return dog.to_dict()
    return {}

def getAllDogs():
    dogs = dogs_ref.stream()
    data = []
    for dog in dogs:
        data.append(dog.to_dict())
    return data

def getLostDogs():
    dogs = dogs_ref.stream()
    data = []
    for dog in dogs:
        if(dog.to_dict()["perdido"]):
            data.append(dog.to_dict())
    return data

def getFoundDogs():
    dogs = dogs_ref.stream()
    data = []
    for dog in dogs:
        if(not dog.to_dict()["perdido"]):
            data.append(dog.to_dict())
    return data
