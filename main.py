# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 15:39:43 2020

@author: DAHIANA
"""
import cv2
import numpy as np
from datetime import datetime
from flask import Flask, jsonify
import firebaseService
from PIL import Image
import base64
import io
from geopy.distance import geodesic
app = Flask(__name__)

@app.route('/')
def hello():
    return "Hello world!"

@app.route('/api/getLost')
def apiGetLost():
    result = firebaseService.getLostDogs()
    return jsonify({"lostDogs": result})

@app.route('/api/getFound')
def apiGetFound():
    result = firebaseService.getFoundDogs()
    return jsonify({"foundDogs": result})



def agregar(imagen):
    im= cv2.resize(imagen,(70,70))
    im = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    fot=[]
    r,c=im.shape
    for i in range(r):
        for j in range(c):
            #print(im[i,j],end=",")
            fot.append(str(im[i,j]))
    return fot    

def sortA(a,b):
    n=len(a)
    for i in range(n):
        for j in range(n - i - 1):
            if a[j] < a[j + 1]:
                a[j], a[j + 1] = a[j + 1], a[j]
                b[j], b[j + 1] = b[j + 1], b[j]    
    return a,b

def compararR(perdido,encontrado,parecidos):
    #print("compararR","perdido",perdido['id'],"encontrado",encontrado['id'])
    #0cola-1descripcion-2fecha-3foto-4id-5pelaje-6perdido-7persona-8raza-9sexo-10tamaño
    tamaño=["Pequeño", "Mediano", "Grande", "Muy Grande"]
    sexo=["Macho", "Hembra", "No se conoce"]
    raza=["golden retriver","braco de weimar","chihuahua","bulldog",
          "yorkshire terrier","pastor aleman","french poodle","pug","labrador",
          "schnauzer","pincher","shih tzu","pitbull","bull terrier","beagle",
          "american bully","chow chow","husky siberiano","dalmata","otra"]
    #lugar=["a","b","c","d","e","f","g","h"]
    pelaje=["Corto", "Largo"]
    cola=["Corta", "Larga"]
    #fecha=["1","2","3","4","5","6","7"]
    #tipoFoto=["frente","perfil"]
    diff=(encontrado['fecha']-perdido['fecha']).days
    #print(diff)
    coords_1 = (perdido['ubicacion'][0], perdido['ubicacion'][1])
    coords_2 = (encontrado['ubicacion'][0],encontrado['ubicacion'][1])
    distancia = geodesic(coords_1, coords_2).km
    punt=0
    maxPunt = 114
    puntosRaza = 25 if (perdido['raza']=='otra') else 0
    if(tamaño.index(perdido['tamaño'])==tamaño.index(encontrado['tamaño'])):punt+=30
    if(abs(tamaño.index(perdido['tamaño'])-tamaño.index(encontrado['tamaño'])) == 1):punt+=10
    if(sexo.index(perdido['sexo'])==sexo.index(encontrado['sexo'])):punt+=5
    if(sexo.index(encontrado['sexo'])==2):punt+=3
    if(perdido['raza']!='otra' and raza.index(perdido['raza'])==raza.index(encontrado['raza'])):punt+=50
    if(distancia <= 1.0):punt+=40
    if(distancia > 1.0 and distancia < 2.5):punt+=20
    if(pelaje.index(perdido['pelaje'])==pelaje.index(encontrado['pelaje'])):punt+=(15 + puntosRaza)
    if(abs(pelaje.index(perdido['pelaje'])-pelaje.index(encontrado['pelaje'])) == 1):punt+=(13 + puntosRaza)
    if(cola.index(perdido['cola'])==cola.index(encontrado['cola'])):punt+=(14 + puntosRaza)
    #if(descripcion.index(per[7])==descripcion.index(todos[7])):punt+=5
    if(diff<0):punt-=500
    if(diff<60 and diff>=0):punt+=10
    if(diff>=60):punt+=4
    parecidos.append(round(punt*100/maxPunt, 2))#Cambiar el 94 por la suma mayor de las punt
    
def compararF(perdido,listaEncontrados,resultF):
    #print("compararR","perdido",perdido['id'],"encontrado",encontrado['id'])
    #0cola-1descripcion-2fecha-3foto-4id-5pelaje-6perdido-7persona-8raza-9sexo-10tamaño
    imagenPerdido = perdido['foto']
    base64_decodedPerdido = base64.b64decode(imagenPerdido)
    image = Image.open(io.BytesIO(base64_decodedPerdido))
    imagenPerdido = cv2.cvtColor(np.asarray(image),cv2.COLOR_RGB2BGR)  
    imagenPerdido = agregar(imagenPerdido)
    r=70
    c=70
    perros=[]
    #print("IMAGEN PERDIDO",imagenPerdido)
    for encontrado in listaEncontrados:
        imagenEncontrado = encontrado['foto']
        base64_decodedPerdido = base64.b64decode(imagenEncontrado)
        image = Image.open(io.BytesIO(base64_decodedPerdido))
        imagenEncontrado = cv2.cvtColor(np.asarray(image),cv2.COLOR_RGB2BGR) 
        imagenEncontrado = agregar(imagenEncontrado)
        perros.append(imagenEncontrado)
    for p in perros:
        pxy=-1
        cont=0
        listaEncontrados=r*c +1
        for i in range(r):
            for j in range(c):
                pxy+=1
                if(int(imagenPerdido[pxy])<15 or int(p[pxy])<15):
                    listaEncontrados-=1
                    continue
                elif(int(p[pxy])>240 or int(imagenPerdido[pxy])>240):
                    listaEncontrados-=1
                    continue
                elif(abs(int(imagenPerdido[pxy])-int(p[pxy]))<100):
                    cont+=1
        if(listaEncontrados==0):continue
        resultF.append(round(cont*100/listaEncontrados , 2))  
    return resultF


@app.route('/api/search/<int:id>', methods=['POST'])
def search(id):
    #print("search  id=",id)
    parecidos=[]
    mejoresR=[]
    data=[]
    dogToCompare = firebaseService.getDogById(id)
    listOfFound = firebaseService.getFoundDogs()

    for dogFound in listOfFound:
        data.append(dogFound)
        compararR(dogToCompare,dogFound,parecidos)
    sortA(parecidos,data)
        #print("\ndespues de organizar\n")

    print("SR",parecidos[:10])

    for i in range(10):
        if(i==len(data)):break
        mejoresR.append(data[i])
    resultF=[]
    data1= data[:10].copy() if len(data)>10 else data[:].copy()

    
    compararF(dogToCompare,mejoresR,resultF)
    print("\n")
    sortA(resultF,data1)
    print("FOTOS",resultF)
    salida=[]
    for i in range(len(mejoresR)):
        salida.append(round(parecidos[i]*0.5+resultF[data1.index(mejoresR[i])]*0.5,1))        
    print("final",salida)
    sortA(salida,data)
    print("FINAL ordenado")
    print(salida)
    print("SALIDA")
    print("SEÑOR USUARIO")
    print("Para su mascota perdida",end=" ")
    print("Los perrros similares que se han encontrado son:")
    for i in data[:5]:
        print(i['id'])
    return jsonify({"parecidos": data[:5]})
    
if __name__ == '__main__':
    app.run(debug=False)
    
